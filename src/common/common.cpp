/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <string.h>
#include <ctype.h>

#include "common_defs.h"
//------------------------------------------------------------------------------------------------------------------------------
// List of camera APIs that could be used to access the camera
//------------------------------------------------------------------------------------------------------------------------------
const char* GetApiString(int api){
    switch ((CameraAPI)api){
        case CAMAPI_V4L2: return "v4l2";
        case CAMAPI_HAL3: return "hal3";
        default:          return "Invalid";
    }
}

//------------------------------------------------------------------------------------------------------------------------------
// List of camera APIs that could be used to access the camera
//------------------------------------------------------------------------------------------------------------------------------
const char* GetCamModeString(int api){
    switch ((CameraMode)api){
        case CAMMODE_PREVIEW:  return "preview";
        case CAMMODE_VIDEO:    return "video";
        case CAMMODE_SNAPSHOT: return "snapshot";
        default:               return "Invalid";
    }
}

// -----------------------------------------------------------------------------------------------------------------------------
// Supported preview formats
// -----------------------------------------------------------------------------------------------------------------------------
const char* GetPreviewFmtString(int fmt){
    switch ((PreviewFormat)fmt){
        case PREVIEW_FMT_RAW8:  return "raw8";
        case PREVIEW_FMT_RAW10: return "raw10";
        case PREVIEW_FMT_NV12:  return "nv12";
        case PREVIEW_FMT_NV21:  return "nv21";
        case PREVIEW_FMT_BLOB:  return "blob";
        default:                return "Invalid";
    }
}

// -----------------------------------------------------------------------------------------------------------------------------
// Supported video formats
// -----------------------------------------------------------------------------------------------------------------------------
const char* GetVideoFmtString(int fmt){
    switch ((VideoFormat)fmt){
        case VIDEO_FMT_H264: return "h264";
        case VIDEO_FMT_H265: return "h265";
        default:             return "Invalid";
    }
}

// -----------------------------------------------------------------------------------------------------------------------------
// Supported snapshot formats
// -----------------------------------------------------------------------------------------------------------------------------
const char* GetSnapshotFmtString(int fmt){
    switch ((SnapshotFormat)fmt){
        case SNAPSHOT_FMT_JPG: return "jpg";
        default:               return "Invalid";
    }
}

//------------------------------------------------------------------------------------------------------------------------------
// List of camera types
//------------------------------------------------------------------------------------------------------------------------------
// Get the string associated with the type
const char* GetTypeString(int type)
{
    switch ((CameraType)type){
        case CAMTYPE_TRACKING: return "tracking";
        case CAMTYPE_HIRES:    return "hires";
        case CAMTYPE_TOF:      return "tof";
        case CAMTYPE_STEREO:   return "stereo";
        default:               return "Invalid";
    }
}

// Get the type associated with the string
const CameraType GetCameraTypeFromString(char *type)
{

    char lowerType[strlen(type) + 5];
    int i;
    for(i = 0; type[i]; i++){
      lowerType[i] = tolower(type[i]);
    }
    lowerType[i] = 0;

    for(int i = 0; i < CAMTYPE_MAX_TYPES; i++){
        if(!strcmp(lowerType, GetTypeString((CameraType)i))){
            return (CameraType)i;
        }
    }

    return CAMTYPE_INVALID;
}

//------------------------------------------------------------------------------------------------------------------------------
// Auto exposure algorithms
//------------------------------------------------------------------------------------------------------------------------------
const char* GetAEString(int algo)
{
    switch ((CameraAEAlgo) algo){
        case CAMAEALGO_OFF:   return "off";
        case CAMAEALGO_MVCPA: return "mvcpa";
        case CAMAEALGO_ISP:   return "isp";
        default:              return "Invalid";
    }
}

const char *tofChannelNames[NUM_TOF_CHANNELS] = 
{
    "_ir/",
    "_depth/",
    "_confidence/",
    "_noise/",
    "_pc/"
};