/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <cmath>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <modal_json.h>

#include "common_tools.h"
#include "camera_config.h"
#include "camera_defaults.h"
#include "debug_log.h"


// Note: C++14 requires us to declare it outside even though it has been defined in the class
double          ConfigFile::m_sFileVersion;
PerCameraInfo   ConfigFile::m_sPerCameraInfo[ConfigFile::MaxSupportedCameras] = { 0 };
int32_t         ConfigFile::m_sNumCameras;
std::map<std::string, int> ConfigFile::m_sFmtMap[3];

// -----------------------------------------------------------------------------------------------------------------------------
// Check if the config file version is supported. The config file has a version in it and the implementation in this file
// supports config files with specific version number. So for e.g.  new fields may be added into the config file but the
// code in this file may not support reading the new fields. This function prevent a mismatch between what is supported in
// voxl-camera-server vs what is in the config file.
// -----------------------------------------------------------------------------------------------------------------------------
bool ConfigFile::IsConfigFileVersionSupported(cJSON*  pJsonParent)  ///< Json linked list
{
    bool status = true;

    json_fetch_double_with_default(pJsonParent, JsonVersionString, &m_sFileVersion, NAN);

    if (m_sFileVersion == NAN)
    {
        VOXL_LOG_FATAL("------voxl-camera-server ERROR: config file missing version entry\n");
        status = false;
    }
    else
    {
        if (m_sFileVersion > SupportedVersion)
        {
            VOXL_LOG_FATAL("------ voxl-camera-server ERROR: Config file version <%0.2lf> not supported\n", m_sFileVersion);
            VOXL_LOG_FATAL("------ Supported config file version is <%0.2lf> or lower\n", SupportedVersion);

            m_sFileVersion = 0.0;
            status         = false;
        }
    }

    return status;
}

// -----------------------------------------------------------------------------------------------------------------------------
// Gets the camera type from the config file
// -----------------------------------------------------------------------------------------------------------------------------
CameraType ConfigFile::GetCameraType(cJSON* pJsonCameraPort)    ///< Json linked list=
{
    CameraType camType = CAMTYPE_INVALID;

    char typeString[MAX_NAME_LENGTH];

    json_fetch_string_with_default(pJsonCameraPort, JsonTypeString, typeString, MAX_NAME_LENGTH, DoesNotExistString);

    if (strcmp(typeString, DoesNotExistString))
    {
        for (int i = 0; i < CAMTYPE_MAX_TYPES; i++)
        {
            if (!strcmp(typeString, GetTypeString(i)))
            {
                camType = static_cast<CameraType>(i);
                break;
            }
        }
    }
    return camType;
}

// -----------------------------------------------------------------------------------------------------------------------------
// Gets the camera frame format from the config file
// -----------------------------------------------------------------------------------------------------------------------------
int32_t ConfigFile::GetV01CameraFormat(CameraMode mode, cJSON* pCameraInfo)
{
    int32_t format = -1;
    char    formatStr[MAX_NAME_LENGTH];

    switch(mode)
    {
        case CAMMODE_PREVIEW:
        {
            json_fetch_string_with_default(pCameraInfo, JsonPFormatString, formatStr, MAX_NAME_LENGTH, DoesNotExistString);

            std::string formatString = formatStr;
            if (strcmp(formatStr, DoesNotExistString) && strcmp(formatStr, "disabled"))
            {
                format = m_sFmtMap[0][formatString];
            }
            break;
        }
        case CAMMODE_VIDEO:
        {
            json_fetch_string_with_default(pCameraInfo, JsonVFormatString, formatStr, MAX_NAME_LENGTH, DoesNotExistString);

            std::string formatString = formatStr;
            if (strcmp(formatStr, DoesNotExistString) && strcmp(formatStr, "disabled"))
            {
                format = m_sFmtMap[1][formatString];
            }
            break;
        }
        case CAMMODE_SNAPSHOT:
        {
            json_fetch_string_with_default(pCameraInfo, JsonSFormatString, formatStr, MAX_NAME_LENGTH, DoesNotExistString);

            std::string formatString = formatStr;
            if (strcmp(formatStr, DoesNotExistString) && strcmp(formatStr, "disabled"))
            {
                format = m_sFmtMap[2][formatString];
            }
            break;
        }
        default:
            break;
    }

    if (format == -1)
    {
        if (strcmp(formatStr, DoesNotExistString) && strcmp(&formatStr[0], "disabled"))
        {
            VOXL_LOG_FATAL("------ voxl-camera-server ERROR: Invalid camera fmt <%s> in the config file\n", formatStr);
        }
    }

    return (int32_t)format;
}

// -----------------------------------------------------------------------------------------------------------------------------
// Gets the camera api from the config file
// -----------------------------------------------------------------------------------------------------------------------------
CameraAPI ConfigFile::GetCameraApi(cJSON* pCameraInfo)
{
    CameraAPI camApi = CAMAPI_INVALID;
    char      apiString[MAX_NAME_LENGTH];

    json_fetch_string_with_default(pCameraInfo, JsonApiString, &apiString[0], MAX_NAME_LENGTH, DoesNotExistString);

    if (strcmp(&apiString[0], DoesNotExistString))
    {
        for (int i = 0; i < CAMAPI_MAX_TYPES; i++)
        {
            if (!strcmp(&apiString[0], GetApiString(i)))
            {
                camApi = static_cast<CameraAPI>(i);
                break;
            }
        }
    }

    if (camApi == CAMAPI_INVALID)
    {
        VOXL_LOG_FATAL("------ voxl-camera-server ERROR: Invalid Api name <%s> in the config file\n", &apiString[0]);
    }

    return camApi;
}

// -----------------------------------------------------------------------------------------------------------------------------
// Gets the auto exposure algorithm for the camera config. If this function is called it is assumed that such an entry exists.
// Caller should not call this function if they know that such an entry may not be there - for e.g. if the autoexposure mode is
// set to "on" - the 'mode_off_algorithm" field is a don't care and should not be read/used
// -----------------------------------------------------------------------------------------------------------------------------
CameraAEAlgo ConfigFile::GetCameraAEAlgo(cJSON* pAutoExpInfo)
{
    CameraAEAlgo camAEalgo = CAMAEALGO_INVALID;
    char         aeString[MAX_NAME_LENGTH];

    json_fetch_string_with_default(pAutoExpInfo, JsonAutoExposureString, &aeString[0], MAX_NAME_LENGTH, DoesNotExistString);

    if (strcmp(&aeString[0], DoesNotExistString))
    {
        for (int i = 0; i < CAMAEALGO_MAX_TYPES; i++)
        {
            if (!strcmp(&aeString[0], GetAEString(i)))
            {
                camAEalgo = static_cast<CameraAEAlgo>(i);
                break;
            }
        }
    }

    if (camAEalgo == CAMAEALGO_INVALID)
    {
        VOXL_LOG_FATAL("------ voxl-camera-server ERROR: Invalid AE mode <%s> in the config file\n", &aeString[0]);
    }

    return camAEalgo;
}

// -----------------------------------------------------------------------------------------------------------------------------
// Gets the integer values associated with a particular element
// -----------------------------------------------------------------------------------------------------------------------------
int ConfigFile::GetIntValue(cJSON*      pCameraInfo,    ///< Pointer to the entire camera info element
                            const char* pString)        ///< Get the integer value associated with this string element
{
    int valueint = INT_INVALID_VALUE;

    cJSON* pValueInt = cJSON_GetObjectItem(pCameraInfo, pString);

    if (pValueInt != NULL)
    {
        valueint = pValueInt->valueint;
    }
    else
    {
        VOXL_LOG_FATAL("------ voxl-camera-server ERROR: Could not find element %s in the config file\n", pString);
    }

    return valueint;
}

// -----------------------------------------------------------------------------------------------------------------------------
// Reads the stream data from version 0.1 of the config file
// -----------------------------------------------------------------------------------------------------------------------------
void ConfigFile::GetV01Streams(cJSON*          pJsonCamPort,
                               PerCameraInfo*  pPerCameraInfo)    ///< Returned camera info for each camera
{

    int32_t format       = -1;
    format = GetV01CameraFormat(CAMMODE_PREVIEW, pJsonCamPort);
    json_fetch_int_with_default(pJsonCamPort,
                                JsonPWidthString,
                                &pPerCameraInfo->modeInfo[CAMMODE_PREVIEW].width,
                                0);
    json_fetch_int_with_default(pJsonCamPort,
                                JsonPHeightString,
                                &pPerCameraInfo->modeInfo[CAMMODE_PREVIEW].height,
                                0);
    if (format != -1)
    {
        pPerCameraInfo->modeInfo[CAMMODE_PREVIEW].isEnabled = true;
        pPerCameraInfo->modeInfo[CAMMODE_PREVIEW].format = format;
    }
    else
    {
        pPerCameraInfo->modeInfo[CAMMODE_PREVIEW].isEnabled = false;
    }

    format = GetV01CameraFormat(CAMMODE_VIDEO, pJsonCamPort);
    json_fetch_int_with_default(pJsonCamPort,
                                JsonVWidthString,
                                &pPerCameraInfo->modeInfo[CAMMODE_VIDEO].width,
                                0);
    json_fetch_int_with_default(pJsonCamPort,
                                JsonVHeightString,
                                &pPerCameraInfo->modeInfo[CAMMODE_VIDEO].height,
                                0);

    if (format != -1)
    {
        pPerCameraInfo->modeInfo[CAMMODE_VIDEO].isEnabled = true;
        pPerCameraInfo->modeInfo[CAMMODE_VIDEO].format = format;
    }
    else
    {
        pPerCameraInfo->modeInfo[CAMMODE_VIDEO].isEnabled = false;
    }

    format = GetV01CameraFormat(CAMMODE_SNAPSHOT, pJsonCamPort);
    json_fetch_int_with_default(pJsonCamPort,
                                JsonSWidthString,
                                &pPerCameraInfo->modeInfo[CAMMODE_SNAPSHOT].width,
                                0);
    json_fetch_int_with_default(pJsonCamPort,
                                JsonSHeightString,
                                &pPerCameraInfo->modeInfo[CAMMODE_SNAPSHOT].height,
                                0);

    if (format != -1)
    {
        pPerCameraInfo->modeInfo[CAMMODE_SNAPSHOT].isEnabled = true;
        pPerCameraInfo->modeInfo[CAMMODE_SNAPSHOT].format = format;
    }
    else
    {
        pPerCameraInfo->modeInfo[CAMMODE_SNAPSHOT].isEnabled = false;
    }
}

// -----------------------------------------------------------------------------------------------------------------------------
// This function retrieves all the fields for a single camera that is specified in the config file
// -----------------------------------------------------------------------------------------------------------------------------
Status ConfigFile::GetCameraInfo(cJSON*          pJsonParent,       ///< Main Json linked list
                                 const char*     pPortName,         ///< Port on which the camera is connected
                                 PerCameraInfo*  pPerCameraInfo)    ///< Returned camera info for each camera
{
    memset(pPerCameraInfo, 0, sizeof(PerCameraInfo));
    cJSON*  pJsonCamPort = NULL;

    pJsonCamPort = json_fetch_object(pJsonParent, pPortName);

    if (pJsonCamPort != NULL)
    {
        pPerCameraInfo->type                  = GetCameraType(pJsonCamPort);
        //Get Name (defaults to the type)
        json_fetch_string_with_default(pJsonCamPort, 
                                       JsonNameString,
                                       pPerCameraInfo->name,
                                       MAX_NAME_LENGTH,
                                       GetTypeString(pPerCameraInfo->type));

        if(!strcmp(pPerCameraInfo->name, getDefaultCameraInfo(CAMTYPE_MAX_TYPES).name))return S_OK;

        pPerCameraInfo->accessApi             = GetCameraApi(pJsonCamPort);
        pPerCameraInfo->expGainInfo.algorithm = GetCameraAEAlgo(pJsonCamPort);
        if(m_sFileVersion == 0.1){
            GetV01Streams(pJsonCamPort, pPerCameraInfo);

            for(int i = 0; i < 3; i ++){

                if(pPerCameraInfo->modeInfo[i].width == 0){
                    pPerCameraInfo->modeInfo[i].format = -1;
                }

            }

        }else{
            
            for(int i = 0; i < 3; i ++){

                if(!cJSON_HasObjectItem(pJsonCamPort, GetCamModeString(i))){
                    pPerCameraInfo->modeInfo[i].format = -1;
                    continue;
                } 

                cJSON *pJsonMode = json_fetch_object(pJsonCamPort, GetCamModeString(i));
                char formatString[MAX_NAME_LENGTH];
                json_fetch_string_with_default(pJsonMode,
                                               JsonFormatString,
                                               formatString,
                                               MAX_NAME_LENGTH,
                                               "Invalid");
                pPerCameraInfo->modeInfo[i].format = m_sFmtMap[i][formatString];
                json_fetch_int_with_default(pJsonMode,
                                            JsonWidthString,
                                            &pPerCameraInfo->modeInfo[i].width,
                                            INT_INVALID_VALUE);
                json_fetch_int_with_default(pJsonMode,
                                            JsonHeightString,
                                            &pPerCameraInfo->modeInfo[i].height,
                                            INT_INVALID_VALUE);
                json_fetch_bool_with_default(pJsonMode,
                                             JsonEnabledString,
                                             (int*) &pPerCameraInfo->modeInfo[i].isEnabled,
                                             false);
            }

        }
        
        ///<@todo Enable the use of overrideId
        json_fetch_int_with_default   (pJsonCamPort, JsonFpsString,          &pPerCameraInfo->fps,                                 30);
        json_fetch_int_with_default   (pJsonCamPort, JsonTofModeString,      &pPerCameraInfo->tofMode,                             9);
        json_fetch_int_with_default   (pJsonCamPort, JsonOverrideIdString,   &pPerCameraInfo->overrideId,                          -1);
        json_fetch_int_with_default   (pJsonCamPort, JsonRotationString,     (int*)&pPerCameraInfo->rotationAngle,                 0);
        json_fetch_int_with_default   (pJsonCamPort, JsonExposureNsString,   &pPerCameraInfo->expGainInfo.exposureNs,              5259763);
        json_fetch_int_with_default   (pJsonCamPort, JsonGainString,         &pPerCameraInfo->expGainInfo.gain,                    400);
        json_fetch_int_with_default   (pJsonCamPort, JsonCPAFiltSizeString,  &pPerCameraInfo->expGainInfo.mvCPA.cpaFilterSize,     2);
        json_fetch_double_with_default(pJsonCamPort, JsonCPAExpCostString ,  &pPerCameraInfo->expGainInfo.mvCPA.cpaExposureCost,   0.75);
        json_fetch_double_with_default(pJsonCamPort, JsonCPAGainCostString,  &pPerCameraInfo->expGainInfo.mvCPA.cpaGainCost,       0.25);
        json_fetch_bool_with_default  (pJsonCamPort, JsonCPAHistogramString, (int*)&pPerCameraInfo->expGainInfo.mvCPA.histEnabled, false);

        if(pPerCameraInfo->type == CAMTYPE_TOF){
            if(!tofModeSupported(pPerCameraInfo->tofMode)){
                VOXL_LOG_FATAL("------ voxl-camera-server: FATAL Tof Mode: %d not supported!\n",
                                        pPerCameraInfo->tofMode);
                VOXL_LOG_FATAL("\tSupported Modes are: ");
                const int *modes = getSupportedTofModes();
                for(int i = 0; modes[i] != -1; i++){
                    VOXL_LOG_FATAL("%d", modes[i]);
                    if(modes[i+1] == -1)
                        VOXL_LOG_FATAL("\n");
                    else
                        VOXL_LOG_FATAL(", ");
                }
                return S_ERROR;
            }

            if(!tofFrameRateSupported(pPerCameraInfo->tofMode, pPerCameraInfo->fps)){
                VOXL_LOG_FATAL("------ voxl-camera-server: FATAL Tof Framerate: %d not supported!\n",
                                        pPerCameraInfo->fps);
                VOXL_LOG_FATAL("\tSupported Framerates for mode %d are: ",
                                        pPerCameraInfo->tofMode);
                const int *frs = getSupportedTofFramerates(pPerCameraInfo->tofMode);
                for(int i = 0; frs[i] != -1; i++){
                    VOXL_LOG_FATAL("%d", frs[i]);
                    if(frs[i+1] == -1)
                        VOXL_LOG_FATAL("\n");
                    else
                        VOXL_LOG_FATAL(", ");
                }
                return S_ERROR;
            }
        }

        if(m_sFileVersion == 0.1){
            json_fetch_bool_with_default(pJsonCamPort, JsonDisabledString,  (int *)(&pPerCameraInfo->isEnabled), false);
            pPerCameraInfo->isEnabled = !pPerCameraInfo->isEnabled;
        }else{
            json_fetch_bool_with_default(pJsonCamPort, JsonEnabledString,  (int *)(&pPerCameraInfo->isEnabled), false);
        }
        if (pPerCameraInfo->type == CAMTYPE_INVALID)
        {
            VOXL_LOG_FATAL("------ voxl-camera-server: FATAL Config file: Missing/bad value for camera type\n");
            return S_ERROR;
        }
        if (pPerCameraInfo->accessApi == CAMAPI_INVALID)
        {
            VOXL_LOG_FATAL("------ voxl-camera-server: FATAL Config file: Missing/bad value for access api\n");
            return S_ERROR;
        }
        if (pPerCameraInfo->expGainInfo.algorithm == CAMAEALGO_INVALID)
        {
            VOXL_LOG_FATAL("------ voxl-camera-server: FATAL Config file: Missing/bad value for auto_exposure_mode\n");
            return S_ERROR;
        }
        ///<@todo add more error checks
    }
    else
    {
        return S_ERROR;
    }

    VOXL_LOG_INFO("------ voxl-camera-server: Done configuring %s camera\n", pPerCameraInfo->name);

    return S_OK;
}

// -----------------------------------------------------------------------------------------------------------------------------
// Parses the command line arguments to the main function
// -----------------------------------------------------------------------------------------------------------------------------
void ConfigFile::PrintCameraInfo(PerCameraInfo* pCameraInfo)    ///< Camera info
{

    VOXL_LOG_INFO("\t Port       : %s\n", pCameraInfo->port);
    VOXL_LOG_INFO("\t Name       : %s\n", pCameraInfo->name);
    VOXL_LOG_INFO("\t Enabled    : %d\n", pCameraInfo->isEnabled);
    VOXL_LOG_INFO("\t Type       : %s\n", GetTypeString(pCameraInfo->type));

    if(!strcmp(pCameraInfo->name, getDefaultCameraInfo(CAMTYPE_MAX_TYPES).name)){printf("\n");return;}

    VOXL_LOG_INFO("\t Api        : %s\n", GetApiString(pCameraInfo->accessApi));

    if (pCameraInfo->modeInfo[CAMMODE_PREVIEW].isEnabled == true)
    {
        VOXL_LOG_INFO("\t P-W        : %d\n", pCameraInfo->modeInfo[CAMMODE_PREVIEW].width);
        VOXL_LOG_INFO("\t P-H        : %d\n", pCameraInfo->modeInfo[CAMMODE_PREVIEW].height);
        VOXL_LOG_INFO("\t P-Fmt      : %s\n", GetPreviewFmtString(pCameraInfo->modeInfo[CAMMODE_PREVIEW].format));
    }
    if (pCameraInfo->modeInfo[CAMMODE_VIDEO].isEnabled == true)
    {
        VOXL_LOG_INFO("\t V-W        : %d\n", pCameraInfo->modeInfo[CAMMODE_VIDEO].width);
        VOXL_LOG_INFO("\t V-H        : %d\n", pCameraInfo->modeInfo[CAMMODE_VIDEO].height);
        VOXL_LOG_INFO("\t V-Fmt      : %s\n", GetVideoFmtString(pCameraInfo->modeInfo[CAMMODE_VIDEO].format));
    }
    if (pCameraInfo->modeInfo[CAMMODE_SNAPSHOT].isEnabled == true)
    {
        VOXL_LOG_INFO("\t S-W        : %d\n", pCameraInfo->modeInfo[CAMMODE_SNAPSHOT].width);
        VOXL_LOG_INFO("\t S-H        : %d\n", pCameraInfo->modeInfo[CAMMODE_SNAPSHOT].height);
        VOXL_LOG_INFO("\t S-Fmt      : %s\n", GetSnapshotFmtString(pCameraInfo->modeInfo[CAMMODE_SNAPSHOT].format));
    }

    VOXL_LOG_INFO("\t Rotation   : %d\n", pCameraInfo->rotationAngle);
    VOXL_LOG_INFO("\t FPS        : %d\n", pCameraInfo->fps);
    if(pCameraInfo->type == CAMTYPE_TOF)
        VOXL_LOG_INFO("\t Tof Mode   : %d\n", pCameraInfo->tofMode);
    VOXL_LOG_INFO("\t OverrideId : %d\n", pCameraInfo->overrideId);
    VOXL_LOG_INFO("\t AEAlgo     : %s\n", GetAEString(pCameraInfo->expGainInfo.algorithm));

    if (pCameraInfo->expGainInfo.algorithm == CAMAEALGO_MVCPA)
    {
        VOXL_LOG_INFO("\t mvcpa_filter_size    : %d\n", pCameraInfo->expGainInfo.mvCPA.cpaFilterSize);
        VOXL_LOG_INFO("\t mvcpa_exp_cost       : %0.4f\n", (float)pCameraInfo->expGainInfo.mvCPA.cpaExposureCost);
        VOXL_LOG_INFO("\t mvcpa_gain_cost      : %0.4f\n", (float)pCameraInfo->expGainInfo.mvCPA.cpaGainCost);
        VOXL_LOG_INFO("\t mvcpa_histogram      : %s\n", bool_string(pCameraInfo->expGainInfo.mvCPA.histEnabled));
    }
    else if (pCameraInfo->expGainInfo.algorithm == CAMAEALGO_OFF)
    {
        VOXL_LOG_INFO("\t Exp        : %d\n", pCameraInfo->expGainInfo.exposureNs);
        VOXL_LOG_INFO("\t Gain       : %d\n", pCameraInfo->expGainInfo.gain);
    }

    VOXL_LOG_INFO("\n");
}

// -----------------------------------------------------------------------------------------------------------------------------
// Read and parse the config file. This function can be modified to support any config file format. The information for each
// camera read from the config file is returned from this function.
//
// Note:
// "ppPerCameraInfo" will point to memory allocated by this function and it is the callers responsibility to free it.
// -----------------------------------------------------------------------------------------------------------------------------
Status ConfigFile::Read(const char*     pConfigFileName,    ///< Config filename
                        uint32_t*       pNumCameras,        ///< Returned number of cameras detected in the config file
                        PerCameraInfo** ppPerCameraInfo)    ///< Returned camera info for each camera in the config file
{
    cJSON* pJsonParent;

    m_sFmtMap[0]["raw8"]  = PREVIEW_FMT_RAW8;
    m_sFmtMap[0]["raw10"] = PREVIEW_FMT_RAW10;
    m_sFmtMap[0]["nv12"]  = PREVIEW_FMT_NV12;
    m_sFmtMap[0]["nv21"]  = PREVIEW_FMT_NV21;
    m_sFmtMap[0]["blob"]  = PREVIEW_FMT_BLOB;
    m_sFmtMap[1]["h264"]  = VIDEO_FMT_H265;
    m_sFmtMap[1]["h265"]  = VIDEO_FMT_H265;
    m_sFmtMap[2]["jpg"]   = SNAPSHOT_FMT_JPG;

    pJsonParent = json_read_file(pConfigFileName);

    Status status = S_ERROR;

    if (IsConfigFileVersionSupported(pJsonParent) == false)
    {
        VOXL_LOG_FATAL("------ voxl-camera-server: invalid config file\n");
        status = S_ERROR;
    }
    else
    {
        VOXL_LOG_INFO("------ voxl-camera-server INFO: Port J2\n\n");
        status = GetCameraInfo(pJsonParent, JsonPortJ2String, &m_sPerCameraInfo[m_sNumCameras]);

        if (status == S_OK)
        {
            strcpy(&m_sPerCameraInfo[m_sNumCameras].port[0], JsonPortJ2String);
            PrintCameraInfo(&m_sPerCameraInfo[m_sNumCameras]);
            m_sNumCameras++;

        }else return S_ERROR;

        VOXL_LOG_INFO("------ voxl-camera-server INFO: Port J3\n\n");
        status = GetCameraInfo(pJsonParent, JsonPortJ3String, &m_sPerCameraInfo[m_sNumCameras]);

        if (status == S_OK)
        {
            strcpy(&m_sPerCameraInfo[m_sNumCameras].port[0], JsonPortJ3String);
            PrintCameraInfo(&m_sPerCameraInfo[m_sNumCameras]);
            m_sNumCameras++;
        }else return S_ERROR;

        VOXL_LOG_INFO("------ voxl-camera-server INFO: Port J4\n\n");
        status = GetCameraInfo(pJsonParent, JsonPortJ4String, &m_sPerCameraInfo[m_sNumCameras]);

        if (status == S_OK)
        {
            strcpy(&m_sPerCameraInfo[m_sNumCameras].port[0], JsonPortJ4String);
            PrintCameraInfo(&m_sPerCameraInfo[m_sNumCameras]);
            m_sNumCameras++;
        }else return S_ERROR;
    }

    *pNumCameras     = m_sNumCameras;
    *ppPerCameraInfo = new PerCameraInfo[m_sNumCameras];

    memcpy(*ppPerCameraInfo, m_sPerCameraInfo, m_sNumCameras*sizeof(PerCameraInfo));

    if(m_sFileVersion != SupportedVersion){
        VOXL_LOG_INFO("Old config file version detected, writing settings to current version\n");
        Write(pConfigFileName,
            m_sNumCameras,
            *ppPerCameraInfo);
    }


    return S_OK;
}

// -----------------------------------------------------------------------------------------------------------------------------
// Read and parse the config file. This function can be modified to support any config file format. The information for each
// camera read from the config file is returned from this function.
//
// -----------------------------------------------------------------------------------------------------------------------------
Status ConfigFile::Write(const char*     pConfigFileName,      ///< Config filename
                         int             pNumCameras,          ///< Number of cameras
                         PerCameraInfo   pPerCameraInfo[])     ///< Camera info for each camera in the config file
{

    Status returnStatus = S_ERROR;
    cJSON* head = cJSON_CreateObject();

    cJSON_AddNumberToObject(head, JsonVersionString, SupportedVersion);

    for(int i = 0; i < pNumCameras; i++){

        strcpy(pPerCameraInfo[i].port, JsonPortStrings(i));

        PerCameraInfo info = pPerCameraInfo[i];
        cJSON *camera = cJSON_AddObjectToObject(head, pPerCameraInfo[i].port);
        cJSON_AddStringToObject(camera, JsonNameString, info.name);
        cJSON_AddBoolToObject(camera, JsonEnabledString, info.isEnabled);

        if(!info.isEnabled && !strcmp(info.name, getDefaultCameraInfo(CAMTYPE_MAX_TYPES).name)) continue;

        cJSON_AddStringToObject(camera, JsonTypeString, GetTypeString(info.type));
        cJSON_AddStringToObject(camera, JsonApiString, GetApiString(info.accessApi));
        cJSON_AddNumberToObject(camera, JsonRotationString, (int) info.rotationAngle);
        if(info.type == CAMTYPE_TOF)
            cJSON_AddNumberToObject(camera, JsonTofModeString, info.tofMode);
        cJSON_AddNumberToObject(camera, JsonFpsString,  info.fps);
        cJSON_AddNumberToObject(camera, JsonOverrideIdString, info.overrideId);
        cJSON_AddStringToObject(camera, JsonAutoExposureString, GetAEString(info.expGainInfo.algorithm));
        if(info.expGainInfo.exposureNs != -1 && info.expGainInfo.exposureNs != 5259763) 
            cJSON_AddNumberToObject(camera, JsonExposureNsString, info.expGainInfo.exposureNs);
        if(info.expGainInfo.gain != -1 && info.expGainInfo.gain != 400) 
            cJSON_AddNumberToObject(camera, JsonGainString, info.expGainInfo.gain);


        if(info.expGainInfo.algorithm == CAMAEALGO_MVCPA){
            cJSON_AddNumberToObject(camera, JsonCPAFiltSizeString,  info.expGainInfo.mvCPA.cpaFilterSize);
            cJSON_AddNumberToObject(camera, JsonCPAExpCostString,   info.expGainInfo.mvCPA.cpaExposureCost);
            cJSON_AddNumberToObject(camera, JsonCPAGainCostString,  info.expGainInfo.mvCPA.cpaGainCost);
            cJSON_AddBoolToObject  (camera, JsonCPAHistogramString, info.expGainInfo.mvCPA.histEnabled);
        }

        const char *streamNames[3] = {JsonPreviewString, JsonVideoString, JsonSnapshotString};
        for(int j = 0; j < 3; j++){

            if(info.modeInfo[j].format != -1){
                cJSON *stream = cJSON_AddObjectToObject(camera, streamNames[j]);

                cJSON_AddBoolToObject(stream, JsonEnabledString, info.modeInfo[j].isEnabled);
                cJSON_AddNumberToObject(stream, JsonWidthString, info.modeInfo[j].width);
                cJSON_AddNumberToObject(stream, JsonHeightString, info.modeInfo[j].height);
                switch (j){
                    case 0:
                        cJSON_AddStringToObject(stream, JsonFormatString,
                                                GetPreviewFmtString(info.modeInfo[j].format));
                        break;
                    case 1:
                        cJSON_AddStringToObject(stream, JsonFormatString,
                                                GetVideoFmtString(info.modeInfo[j].format));
                        break;
                    case 2:
                        cJSON_AddStringToObject(stream, JsonFormatString,
                                                GetSnapshotFmtString(info.modeInfo[j].format));
                        break;
                }
            }
        }

    }

    FILE *file = fopen(pConfigFileName, "w");
    if(file == NULL){

        VOXL_LOG_FATAL("Error opening config file: %s to write to\n", pConfigFileName);

    }else{
        char *jsonString = cJSON_Print(head);

        VOXL_LOG_INFO("Writing new configuration to %s:\n%s\n",pConfigFileName, jsonString);
        fwrite(jsonString, 1, strlen(jsonString), file);
        fclose(file);
    }

    cJSON_Delete(head);

    return returnStatus;
}
