/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <string.h>
#include "common_defs.h"
#include "debug_log.h"
#include "expgain_interface_generic.h"
#include "mvCPA_interface.h"
#include "mvCPA.h"

// -----------------------------------------------------------------------------------------------------------------------------
// Performs any one time initialization. This function should only be called once.
// -----------------------------------------------------------------------------------------------------------------------------
Status MVCPAExpGain::Initialize(const ExpGainInterfaceData* pIntfData)
{
    Status status = S_OK;

    if (pIntfData != NULL)
    {
        memcpy(&m_expGainIntfData, pIntfData, sizeof(ExpGainInterfaceData));
        memcpy(&m_mvcpaConfig,     pIntfData->pAlgoSpecificData, sizeof(MVCPAData));

        if(m_mvcpaConfig.histEnabled){
            m_config.cpaType                           = MVCPA_MODE_HISTOGRAM;
        }else{
            m_config.cpaType                           = MVCPA_MODE_COST;
        }

        ///<@todo fix these hardcoded values
        m_config.width                             = pIntfData->width;
        m_config.height                            = pIntfData->height;
        m_config.format                            = MVCPA_FORMAT_GRAY8;
        m_config.legacyCost.startExposure          = 0.2f;
        m_config.legacyCost.startGain              = 0.3f;
        m_config.legacyCost.filterSize             = m_mvcpaConfig.cpaFilterSize;
        m_config.legacyCost.gainCost               = m_mvcpaConfig.cpaGainCost;
        m_config.legacyCost.exposureCost           = m_mvcpaConfig.cpaExposureCost;
        m_config.legacyCost.enableHistogramCost    = false;
        m_config.legacyCost.thresholdUnderflowed   = 0;
        m_config.legacyCost.thresholdSaturated     = 255;
        m_config.legacyCost.systemBrightnessMargin = 0;
        m_config.histogram.exposureMin             = 0.001f;
        m_config.histogram.exposureSoftMax         = 0.2f;
        m_config.histogram.exposureMax             = 0.2f;
        m_config.histogram.gainMin                 = 0.001f;
        m_config.histogram.gainSoftMax             = 0.3f;
        m_config.histogram.gainMax                 = 1.0f;
        m_config.histogram.logEGPStepSizeMin       = 0.001f;
        m_config.histogram.logEGPStepSizeMax       = 1.0f;

        m_pmvCPA = mvCPA_Initialize(&m_config);

        if (m_pmvCPA == NULL)
        {
            status = S_ERROR;
            VOXL_LOG_FATAL("------ voxl-camera-server FATAL: cannot initialize mvCPA interface\n");
        }
    }

    return status;
}

// -----------------------------------------------------------------------------------------------------------------------------
// This function performs any clean up tasks prior to object destruction
// -----------------------------------------------------------------------------------------------------------------------------
void MVCPAExpGain::Destroy()
{
    if (m_pmvCPA != NULL)
    {
        mvCPA_Deinitialize(m_pmvCPA);
    }
}

// -----------------------------------------------------------------------------------------------------------------------------
// Adds a frame to be used for calculating new values of exposure/gain
// -----------------------------------------------------------------------------------------------------------------------------
Status MVCPAExpGain::GetNewExpGain(const ExpGainFrameData* pNewFrame, ExpGainResult* pNewExpGain)
{
    float cpaExposure = 0.0;
    float cpuGain     = 0.0;

    ///<@todo Remember for stereo pixels we need to divide the stride by 2 - should we do it here or the client does it?
    mvCPA_AddFrame(m_pmvCPA, pNewFrame->pFramePixels, m_expGainIntfData.strideInPixels);
    mvCPA_GetValues(m_pmvCPA, &cpaExposure, &cpuGain);

    pNewExpGain->exposure = MinExposure + (cpaExposure * (MaxExposure - MinExposure));
    pNewExpGain->gain     = MinGain + (cpuGain * (MaxGain - MinGain));

    return S_OK;
}