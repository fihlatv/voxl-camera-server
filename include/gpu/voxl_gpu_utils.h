/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
#ifndef VOXL_GPU_UTILS
#define VOXL_GPU_UTILS

#include "common_defs.h"
#include "debug_log.h"

#define __CL_ENABLE_EXCEPTIONS
#include <CL/cl.hpp>

// Forward Declaration
struct BufferInfo;

// -----------------------------------------------------------------------------------------------------------------------------
// Rotation Api
// -----------------------------------------------------------------------------------------------------------------------------
enum RotationApi
{
    ROTATION_GPU_OPENCL,
};

// -----------------------------------------------------------------------------------------------------------------------------
// Initialization structure
// -----------------------------------------------------------------------------------------------------------------------------
struct RotationCreateData
{
    RotationApi   api;      ///< Api to use
    RotationAngle angle;    ///< Rotation angle
};

// -----------------------------------------------------------------------------------------------------------------------------
// Execute rotation structure
// -----------------------------------------------------------------------------------------------------------------------------
struct RotationExecData
{
    BufferInfo* pSrcBuffer;     ///< Source buffer to rotate
    BufferInfo* pDstBuffer;     ///< Dest buffer to copy the rotated image into
};

// -----------------------------------------------------------------------------------------------------------------------------
// Split Api
// -----------------------------------------------------------------------------------------------------------------------------
enum SplitApi
{
    SPLIT_GPU_OPENCL,
};

// -----------------------------------------------------------------------------------------------------------------------------
// Initialization structure
// -----------------------------------------------------------------------------------------------------------------------------
struct StereoSplitCreateData
{
    SplitApi      api;      ///< Api to use
    PreviewFormat format;   ///<@todo Need to support other formats
};

// -----------------------------------------------------------------------------------------------------------------------------
// Execute rotation structure
// -----------------------------------------------------------------------------------------------------------------------------
struct StereoSplitExecData
{
    unsigned int* pSrcBuffer;       ///< Source buffer to split
    unsigned int  width;            ///< Source buffer width
    unsigned int  height;           ///< Source buffer height
    unsigned int  stride;           ///< Source buffer stride
    unsigned int  sizeBytes;        ///< Size in bytes
    unsigned int* pDstLBuffer;      ///< Dest left buffer
    unsigned int* pDstRBuffer;      ///< Dest right buffer
};

// -----------------------------------------------------------------------------------------------------------------------------
// Class that performs image rotation
// @todo only 180 degree rotation supported currently
// -----------------------------------------------------------------------------------------------------------------------------
class ImageRotation
{
public:
    // Create an instance of the class
    static ImageRotation* Create(RotationCreateData* pCreateData);
    // Main function that does the rotation
    virtual Status Rotate(RotationExecData* pExecData) = 0;

protected:
    ImageRotation() { }
    virtual ~ImageRotation() { }

    RotationAngle m_rotationAngle;  ///< Rotation angle

private:
    // One time initialization
    virtual Status Initialize(RotationCreateData* pCreateData) = 0;
};

// -----------------------------------------------------------------------------------------------------------------------------
// Class that performs image rotation
// @todo only 180 degree rotation supported currently
// -----------------------------------------------------------------------------------------------------------------------------
class GpuRotation : public ImageRotation
{
public:
    GpuRotation() { }
    ~GpuRotation() { }
    // One time initialization
    Status Initialize(RotationCreateData* pInitData);
    // Main function that does the rotation
    Status Rotate(RotationExecData* pExecData);

private:
    static const char RotateCLKernel[];             ///< Kernel code

    std::vector<cl::Platform> m_platform;           ///< Platform
    std::vector<cl::Device>   m_devices;            ///< Devices
    cl::Context*              m_pContext;           ///< CL Context
    cl::CommandQueue*         m_pQueue;             ///< Queue
    cl::Program*              m_pProgram;           ///< Program
    cl::Kernel*               m_pKernel;            ///< CL Kernel
    cl::Buffer*               m_pBufferSrcY;        ///< Src Y data
    cl::Buffer*               m_pBufferDstY;        ///< Dst Y data
    cl::Buffer*               m_pBufferSrcUV;       ///< Src UV data
    cl::Buffer*               m_pBufferDstUV;       ///< Dst UV data
};

// -----------------------------------------------------------------------------------------------------------------------------
// Class that performs stereo image split
// -----------------------------------------------------------------------------------------------------------------------------
class StereoImageSplit
{
public:
    // Create an instance of the class
    static StereoImageSplit* Create(StereoSplitCreateData* pCreateData);
    // Main function that does the rotation
    virtual Status Split(StereoSplitExecData* pExecData) = 0;

protected:
    StereoImageSplit() { }
    virtual ~StereoImageSplit() { }

private:
    // One time initialization
    virtual Status Initialize(StereoSplitCreateData* pCreateData) = 0;
};

// -----------------------------------------------------------------------------------------------------------------------------
// Class that performs stereo split using GPU
// -----------------------------------------------------------------------------------------------------------------------------
class GpuStereoSplit : public StereoImageSplit
{
public:
    GpuStereoSplit() { }
    ~GpuStereoSplit() { }
    // One time initialization
    Status Initialize(StereoSplitCreateData* pCreateData);
    // Main function that does the split
    Status Split(StereoSplitExecData* pExecData);

private:
    static const char SplitCLKernelRaw10[];         ///< Kernel code
    static const char SplitCLKernelRaw8[];          ///< Kernel code

    std::vector<cl::Platform> m_platform;           ///< Platform
    std::vector<cl::Device>   m_devices;            ///< Devices
    cl::Context*              m_pContext;           ///< CL Context
    cl::CommandQueue*         m_pQueue;             ///< Queue
    cl::Program*              m_pProgram;           ///< Program
    cl::Kernel*               m_pKernel;            ///< CL Kernel
    cl::Buffer*               m_pBufferSrc;         ///< Src Y data
    cl::Buffer*               m_pBufferDstL;        ///< Dst left data
    cl::Buffer*               m_pBufferDstR;        ///< Dst right data
    PreviewFormat             m_previewFormat;      ///< Preview format
};

#endif // #ifndef VOXL_GPU_UTILS